#!/usr/bin/env python
#! -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function
import argparse
import yaml
import pdb
from collections import defaultdict, Counter


def player_to_tuple(player):
    return (player["id"], player["name"])


def tabulate(*args):
    return "\t".join(map(format, args))


def sorting(players, x):
    s1 = sum([players[x][sport][1] for sport in players[x]])
    s2 = sum([players[x][sport][2] for sport in players[x]])
    s3 = sum([players[x][sport][3] for sport in players[x]])
    stotal = s1 + s2 + s3
    return (
        s1,
        s2,
        s3,
        stotal,
        players[x]["chgk"][1],
        players[x]["chgk"][2],
        players[x]["chgk"][3],
        players[x]["brain"][1],
        players[x]["brain"][2],
        players[x]["brain"][3],
        players[x]["si"][1],
        players[x]["si"][2],
        players[x]["si"][3],
        players[x]["ek"][1],
        players[x]["ek"][2],
        players[x]["ek"][3],
    )


def teamsorting(teams, x):
    s1 = sum([teams[x][sport][1] for sport in teams[x]])
    s2 = sum([teams[x][sport][2] for sport in teams[x]])
    s3 = sum([teams[x][sport][3] for sport in teams[x]])
    stotal = s1 + s2 + s3
    return (
        s1,
        s2,
        s3,
        stotal,
        teams[x]["chgk"][1],
        teams[x]["chgk"][2],
        teams[x]["chgk"][3],
        teams[x]["brain"][1],
        teams[x]["brain"][2],
        teams[x]["brain"][3],
        teams[x]["ek"][1],
        teams[x]["ek"][2],
        teams[x]["ek"][3],
    )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    players = defaultdict(lambda: defaultdict(lambda: Counter()))
    teams = defaultdict(lambda: defaultdict(lambda: Counter()))
    teamnames = defaultdict(lambda: set())

    results = yaml.safe_load(open(args.filename))

    for year in sorted(results):
        print("{}: {}".format(year, "\t".join(sorted(results[year].keys()))))
        for sport in ("brain", "ek", "si", "chgk"):
            if sport in results[year]:
                for place in results[year][sport]:
                    team = results[year][sport][place]
                    if "recaps" in team:
                        for player in team["recaps"]:
                            players[player_to_tuple(player)][sport][place] += 1
                        teams[team["id"]][sport][place] += 1
                        teamnames[team["id"]].add(team["name"])
                    else:
                        try:
                            players[player_to_tuple(team)][sport][place] += 1
                        except:
                            pdb.set_trace()

    f = open("players_stats.tsv", "w")
    print(
        tabulate(
            "id",
            "name",
            "T1",
            "T2",
            "T3",
            "C1",
            "C2",
            "C3",
            "B1",
            "B2",
            "B3",
            "S1",
            "S2",
            "S3",
            "E1",
            "E2",
            "E3",
        ),
        file=f,
    )
    for player in sorted(players, key=lambda x: sorting(players, x), reverse=True):
        try:
            print(tabulate(player[0], player[1], *sorting(players, player)), file=f)
        except:
            pdb.set_trace()
    f.close()

    f = open("teams_stats.tsv", "w")
    print(
        tabulate(
            "id",
            "name",
            "T1",
            "T2",
            "T3",
            "C1",
            "C2",
            "C3",
            "B1",
            "B2",
            "B3",
            "E1",
            "E2",
            "E3",
        ),
        file=f,
    )
    for team in sorted(teams, key=lambda x: sorting(teams, x), reverse=True):
        try:
            print(
                tabulate(team, list(teamnames[team])[0], *teamsorting(teams, team)),
                file=f,
            )
        except:
            pdb.set_trace()
    f.close()


if __name__ == "__main__":
    main()
