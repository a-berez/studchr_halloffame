#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, unicode_literals

import codecs
import os
from collections import Counter, defaultdict

import yaml

from stats_from_results import player_to_tuple, sorting


def generate_results(champ, template):
    content = ""
    year = int(champ["_meta"]["date_start"].split("-")[0])
    datestart = champ["_meta"]["date_start"].split(" ")[0]
    dateend = champ["_meta"]["date_end"].split(" ")[0]
    mentioned_teams = set()
    content += "{}, {}{}\n\n".format(
        champ["_meta"]["town"],
        "{}–{}".format(datestart, dateend)
        if datestart != dateend
        else "{}".format(datestart),
        " ([рейтинг](http://rating.chgk.info/tournament/{}))".format(
            champ["_meta"]["idtournament"]
        )
        if "idtournament" in champ["_meta"]
        else "",
    )
    if "chgk" in champ:
        if year < 2020:
            name = "Что? Где? Когда?"
        elif year == 2020:
            name = "Игра в загадки"
        else:
            name = "Основная дисциплина"
        content += f"## {name}\n\n"
        prizes, mentioned_teams = format_prizes(champ["chgk"], mentioned_teams)
        content += prizes
    if "brain" in champ:
        if year < 2020:
            name = "Брейн-ринг"
        else:
            name = "Командная игра на скорость без фальстартов"
        content += f"## {name}\n\n"
        prizes, mentioned_teams = format_prizes(champ["brain"], mentioned_teams)
        content += prizes
    if "ek" in champ:
        content += "## Эрудит-квартет\n\n"
        prizes, mentioned_teams = format_prizes(champ["ek"], mentioned_teams)
        content += prizes
    if "si" in champ:
        content += "## Своя игра\n\n"
        for prize in sorted(champ["si"]):
            content += "{}-е место: {}  \n".format(
                prize, format_player(champ["si"][prize])
            )
    final = template.format(
        date=champ["_meta"]["date_start"].split(" ")[0],
        description="СтудЧР-{}".format(year),
        slug=year,
        title="СтудЧР-{}".format(year) if year != 2020 else "Онлайн-СтудЧР",
        content=content,
    )
    with codecs.open("{}.md".format(year), "w", "utf8") as f:
        f.write(final)


def format_prizes(prizes, mentioned_teams):
    content = ""
    for prize in sorted(prizes):
        content += "{}-е место: {} ({})  \n\n".format(
            int(prize), prizes[prize]["name"], prizes[prize]["town"]
        )
        if prizes[prize]["id"] not in mentioned_teams:
            for player in prizes[prize]["recaps"]:
                content += "  - {}  \n".format(format_player(player))
            mentioned_teams.add(prizes[prize]["id"])
        content += "\n\n"
    return content, mentioned_teams


def format_player(player):
    if isinstance(player, tuple):
        player = {"id": player[0], "name": player[1]}
    return "[{}](http://rating.chgk.info/player/{})".format(
        player["name"], player["id"]
    )


def html_format_player(player):
    if isinstance(player, tuple):
        player = {"id": player[0], "name": player[1]}
    return '<a href="http://rating.chgk.info/player/{}">{}</a>'.format(
        player["id"], player["name"]
    )


def html_tabulate(*args, **kwargs):
    th = kwargs.get("th")
    fra = kwargs.get("fra")
    if not fra:
        return "<tr>" + "".join(map(make_th if th else make_td, args)) + "</tr>"
    else:
        func = make_th if th else make_td
        first = [func(args[0], class_="right_aligned")]
        args = list(map(func, args[1:]))
        return "<tr>" + "".join(first + args) + "</tr>"


def make_td(x, class_=False):
    if not class_:
        return "<td>{}</td>".format(x)
    return '<td class="{}">{}</td>'.format(class_, x)


def make_th(x, class_=False):
    if not class_:
        return "<th>{}</th>".format(x)
    return '<th class="{}">{}</th>'.format(class_, x)


def generate_halloffame(results, template, content):
    players = defaultdict(lambda: defaultdict(lambda: Counter()))
    teams = defaultdict(lambda: defaultdict(lambda: Counter()))
    teamnames = defaultdict(lambda: set())
    for year in sorted(results):
        for sport in ("brain", "ek", "si", "chgk"):
            if sport in results[year]:
                for place in results[year][sport]:
                    team = results[year][sport][place]
                    if "recaps" in team:
                        for player in team["recaps"]:
                            players[player_to_tuple(player)][sport][place] += 1
                        teams[team["id"]][sport][place] += 1
                        teamnames[team["id"]].add(team["name"])
                    else:
                        players[player_to_tuple(team)][sport][place] += 1
    # content += '<table class="responsive">\n'
    content += '<div><table class="rtable rtable--flip">\n<thead>\n'
    content += html_tabulate(
        "Игрок",
        "Σ1",
        "Σ2",
        "Σ3",
        "ΣΣ",
        "Ч1",
        "Ч2",
        "Ч3",
        "Б1",
        "Б2",
        "Б3",
        "С1",
        "С2",
        "С3",
        "Э1",
        "Э2",
        "Э3  \n",
        th=True,
        fra=True,
    )
    content += "</thead>\n<tbody>\n"
    for player in sorted(players, key=lambda x: sorting(players, x), reverse=True):
        content += (
            html_tabulate(
                html_format_player(player), *sorting(players, player), fra=True
            )
            + "  \n"
        )
    content += "</tbody>"
    content += "</table>"
    content += "</div>"
    final = template.format(
        description="Зал славы",
        slug="halloffame",
        title="Зал славы",
        date="2023-09-26",
        content=content,
    )
    return final


def main():
    data = yaml.safe_load(open("results.yaml"))

    with codecs.open("template.md", "r") as f:
        template = f.read()
    with codecs.open("content.txt", "r") as f:
        content = f.read()

    os.chdir("content")
    if not os.path.isdir("results"):
        os.mkdir("results")
    os.chdir("results")
    for champ in data:
        generate_results(data[champ], template)

    with codecs.open("halloffame.md", "w") as f:
        f.write(generate_halloffame(data, template, content))


if __name__ == "__main__":
    main()
