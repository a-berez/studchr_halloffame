#!/usr/bin/env python
# -*- coding: utf-8 -*-
import yaml


def check_player(i, record, place, disc, year):
    if not record.get("id"):
        print(f"player {i} ({year} {disc} {place}): no id")
    if not record.get("name"):
        print(f"player {i} ({year} {disc} {place}): no name")


def check_team(record, place, disc, year):
    if not record.get("id"):
        print(f"{year} {disc} {place}: no id")
    if not record.get("name"):
        print(f"{year} {disc} {place}: no name")
    if not record.get("town"):
        print(f"{year} {disc} {place}: no town")
    if not record.get("recaps"):
        print(f"{year} {disc} {place}: no recaps")
    else:
        for i, player in enumerate(record["recaps"]):
            check_player(i + 1, player, place, disc, year)


GOOD = {1, 2, 3}


def main():
    with open("results.yaml", "r") as f:
        results = yaml.safe_load(f)

    for year in sorted(results):
        disciplines = set(results[year].keys())
        disciplines.discard("_meta")
        for disc in sorted(disciplines):
            places = results[year][disc]
            set_places = {int(x) for x in places}
            set_diff = GOOD - set_places
            if set_diff:
                print(
                    f"the following places are missing for {year} {disc}: {','.join(map(str, set_diff))}"
                )
            for place in sorted(places):
                record = places[place]
                if disc == "si":
                    check_player(place, record, place, disc, year)
                else:
                    check_team(record, place, disc, year)


if __name__ == "__main__":
    main()
