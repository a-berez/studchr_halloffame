#!/usr/bin/env python
#! -*- coding: utf-8 -*-
import pyaml
import pyperclip
import argparse
import requests


def get_results(tourn_id):
    url = f"https://api.rating.chgk.net/tournaments/{tourn_id}/results.json?includeTeamMembers=1"
    return requests.get(url).json()


def transform_res(res):
    return {
        "id": res["team"]["id"],
        "name": res["current"]["name"],
        "town": res["current"]["town"]["name"],
        "recaps": [
            {
                "id": player["player"]["id"],
                "name": f"{player['player']['name']} {player['player']['surname']}",
            }
            for player in res["teamMembers"]
        ],
    }


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("tourn_id")
    args = parser.parse_args()

    info = requests.get(
        f"https://api.rating.chgk.net/tournaments/{args.tourn_id}.json"
    ).json()
    year = info["dateStart"].split("-")[0]
    res = [transform_res(res) for res in get_results(args.tourn_id)]
    with open(f"{year}-{args.tourn_id}.yaml", "wb") as f:
        f.write(pyaml.dumps(res))


if __name__ == "__main__":
    main()
